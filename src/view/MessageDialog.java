package view;


import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class MessageDialog {
    @FXML
    private Label messageLabel;

    private Stage stage;

    public void setStage(Stage stage){
        this.stage=stage;
    }

    public void setMessage(String message){
        messageLabel.setText(message);
    }

    public void handleOkClicked(){
        stage.close();
    }
}
