package view;

import javafx.scene.control.*;
import model.Dataset;
import model.ResultEntity;
import parser.ParserException;
import parser.YType;
import searcher.BinaryTreeSearcher;
import searcher.LinearSearcher;
import searcher.Searcher;
import sort_search.MainApp;
import javafx.fxml.FXML;
import sorter.BubbleSorter;
import sorter.HeapSorter;
import sorter.MergeSorter;
import sorter.Sorter;
import utils.OrderCell;
import utils.YArrayList;
import utils.YUtil;

import java.io.File;

public class MainDialog {
    private MainApp main;
    @FXML
    private TableView<Dataset> datasetTable;
    @FXML
    private TableColumn<Dataset, String> idColumnDataset;
    @FXML
    private TableColumn<Dataset, Number> sizeColumnDataset;
    @FXML
    private TableColumn<Dataset, String> typeColumnDataset;
    @FXML
    private TableColumn<Dataset, String> previewColumnDataset;

    @FXML
    private TableView<ResultEntity> resultTable;
    @FXML
    private TableColumn<ResultEntity, String> idColumnResult;
    @FXML
    private TableColumn<ResultEntity, Number> sizeColumnResult;
    @FXML
    private TableColumn<ResultEntity, String> algorithmColumnResult;
    @FXML
    private TableColumn<ResultEntity, Number> operationCountColumnResult;

    @FXML
    private RadioButton integerRadioButton;
    @FXML
    private RadioButton doubleRadioButton;
    @FXML
    private RadioButton stringRadioButton;

    @FXML
    private CheckBox bubbleCheckBox;
    @FXML
    private CheckBox mergeCheckBox;
    @FXML
    private CheckBox heapCheckBox;
    @FXML
    private CheckBox linearCheckBox;
    @FXML
    private CheckBox binaryTreeCheckBox;
    @FXML
    private TextField searchTargetField;
    @FXML
    private CheckBox autoClear;

    public void setMain(MainApp main){
        this.main = main;
        datasetTable.setItems(main.getDatasets());
        resultTable.setItems(main.getResults());
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the dataset table with the 4 columns.
        sizeColumnDataset.setCellValueFactory(cellData -> cellData.getValue().sizeProperty());
        typeColumnDataset.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        previewColumnDataset.setCellValueFactory(cellData -> cellData.getValue().previewProperty());
        idColumnDataset.setCellFactory((col) -> new OrderCell<Dataset,String>());
        datasetTable.setPlaceholder(new Label("Please press on Import button to import data."));
        // Initialize the result table with the 4 columns.
        sizeColumnResult.setCellValueFactory(cellData -> cellData.getValue().sizeProperty());
        algorithmColumnResult.setCellValueFactory(cellData -> cellData.getValue().algorithmProperty());
        operationCountColumnResult.setCellValueFactory(cellData -> cellData.getValue().operationCountProperty());
        idColumnResult.setCellFactory((col) -> new OrderCell<ResultEntity,String>());
        resultTable.setPlaceholder(new Label("Please press on Sort or Search button to get result."));
    }

    private YType getInputType(){
        YType type=null;
        if (integerRadioButton.isSelected())
            type=YType.YInteger;
        if (doubleRadioButton.isSelected())
            type=YType.YDouble;
        if (stringRadioButton.isSelected())
            type=YType.YString;
        return type;
    }

    private void sortDataset(Sorter sorter, Dataset dataset){
        int size=dataset.getSize();
        YArrayList<Object> elements=dataset.getElements().copy();
        int count=sorter.sort(elements);
        String name=sorter.getName();
        Dataset sortedDataset=new Dataset(elements,dataset.getYType());
        ResultEntity result=new ResultEntity(size,name,count,sortedDataset);
        main.getResults().add(result);
    }

    private void searchDatasetAll(Searcher searcher, Dataset dataset){
        int size=dataset.getSize();
        searcher.setElements(dataset.getElements());
        int count=searcher.searchAll(dataset.getElements());
        String name=searcher.getName();
        YArrayList<Object> results=new YArrayList<>();
        results.add("true");
        Dataset resultDataset=new Dataset(results,YType.YString);
        ResultEntity result=new ResultEntity(size,name,count,resultDataset);
        main.getResults().add(result);
    }

    private void searchDataset(Searcher searcher, Dataset dataset, Object target){
        int size=dataset.getSize();
        searcher.setElements(dataset.getElements());
        boolean ret=searcher.search(target);
        int count=searcher.getSearchCount();
        String name=searcher.getName();
        YArrayList<Object> results=new YArrayList<>();
        results.add(String.valueOf(ret));
        Dataset resultDataset=new Dataset(results,YType.YString);
        ResultEntity result=new ResultEntity(size,name,count,resultDataset);
        main.getResults().add(result);
    }

    private void clearResultTable(){
        if (autoClear.isSelected()){
            main.getResults().clear();
        }
    }

    @FXML
    private void handleImportData(){
        // read input data type from UI
        YType type=getInputType();
        if (type==null){
            main.showMessageDialog("Warning","Please select data type before input.");
            return;
        }
        File file= main.showInputFileDialog();
        if (file==null){
            // if user didn't select a file
            main.showMessageDialog("Warning","Please select a file.");
        }else{
            // if user select a input file
            // read file to a list
            YArrayList<Object> list= YUtil.readFile(file, type.getParser());
            if (list.size()>0){
                Dataset dataset=new Dataset(list,type);
                main.getDatasets().add(dataset);
            }else{
                main.showMessageDialog("Warning","It seems no "+ type.getName() +" in the file. Please try again.");
            }
        }
    }

    @FXML
    private void handleEditData(){
        Dataset selectedDataset = datasetTable.getSelectionModel().getSelectedItem();
        if (selectedDataset ==null){
            // no dataset is selected, show error message
            main.showMessageDialog("Error","Please select a dataset before edit.");
        }else {
            // open a new window to edit the selected dataset
            main.showEditDataDialog(selectedDataset,false);
        }
    }

    @FXML
    private void handleViewResult(){
        ResultEntity selectedResult = resultTable.getSelectionModel().getSelectedItem();
        if (selectedResult ==null){
            // no dataset is selected, show error message
            main.showMessageDialog("Error","Please select a result before view.");
        }else {
            // open a new window to edit the selected dataset
            main.showEditDataDialog(selectedResult.getDataset(),true);
        }
    }

    @FXML
    private void handleSortData() {
        clearResultTable();
        Dataset selectedDataset = datasetTable.getSelectionModel().getSelectedItem();
        if (selectedDataset ==null){
            // no dataset is selected, show error message
            main.showMessageDialog("Error","Please select a dataset before sort.");
            return;
        }
        boolean done=false;
        if (bubbleCheckBox.isSelected()){
            sortDataset(new BubbleSorter(),selectedDataset);
            done=true;
        }
        if (mergeCheckBox.isSelected()){
            sortDataset(new MergeSorter(),selectedDataset);
            done=true;
        }
        if (heapCheckBox.isSelected()){
            sortDataset(new HeapSorter(),selectedDataset);
            done=true;
        }
        if (!done){
            main.showMessageDialog("Error","Please select a sort method before sorting.");
        }
    }

    @FXML
    private void handleSearchData() {
        clearResultTable();
        Dataset selectedDataset = datasetTable.getSelectionModel().getSelectedItem();
        if (selectedDataset ==null){
            // no dataset is selected, show error message
            main.showMessageDialog("Error","Please select a dataset before searching.");
            return;
        }
        Object target;
        try{
            target=selectedDataset.getYType().getParser().parse(searchTargetField.getText());
        }catch (ParserException e){
            // input target data is invalid, show error message
            main.showMessageDialog("Error","Please input a valid value before searching.");
            return;
        }
        boolean done=false;
        if (linearCheckBox.isSelected()){
            searchDataset(new LinearSearcher(),selectedDataset,target);
            done=true;
        }
        if (binaryTreeCheckBox.isSelected()){
            searchDataset(new BinaryTreeSearcher(),selectedDataset,target);
            done=true;
        }
        if (!done){
            main.showMessageDialog("Error","Please select a search method before searching.");
        }
    }

    @FXML
    private void handleSearchAllData() {
        clearResultTable();
        Dataset selectedDataset = datasetTable.getSelectionModel().getSelectedItem();
        if (selectedDataset ==null){
            // no dataset is selected, show error message
            main.showMessageDialog("Error","Please select a dataset before searching.");
            return;
        }
        boolean done=false;
        if (linearCheckBox.isSelected()){
            searchDatasetAll(new LinearSearcher(),selectedDataset);
            done=true;
        }
        if (binaryTreeCheckBox.isSelected()){
            searchDatasetAll(new BinaryTreeSearcher(),selectedDataset);
            done=true;
        }
        if (!done){
            main.showMessageDialog("Error","Please select a search method before searching.");
        }
    }

}
