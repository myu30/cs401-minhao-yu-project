package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Dataset;
import model.DataEntity;
import parser.ParserException;
import parser.YParser;
import utils.OrderCell;
import utils.YArrayList;

public class EditDataDialog {
//    The dataset is being edit.
    private Dataset dataset;
//    The parser used to convert string to the type stored in the dataset.
    private YParser parser;
//    The current stage node.
    private Stage stage;

    private boolean isReadOnly=false;

    private ObservableList<DataEntity> elements= FXCollections.observableArrayList();

    @FXML
    private TableView<DataEntity> elementTable;
    @FXML
    private TableColumn<DataEntity, String> idColumnElement;
    @FXML
    private TableColumn<DataEntity, String> dataColumnElement;
    @FXML
    private TextField indexField;
    @FXML
    private TextField dataField;
    @FXML
    private Button editButton;
    @FXML
    private Button newButton;
    @FXML
    private Button insertButton;
    @FXML
    private Button deleteButton;

    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        dataColumnElement.setCellValueFactory(cellData -> cellData.getValue().stringProperty());
        elementTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showElement(newValue));
        idColumnElement.setCellFactory((col) -> new OrderCell<DataEntity,String>());
    }

    private void showElement(DataEntity element){
        if (element==null){
            dataField.setText("");
            indexField.setText("");
        }else{
            // display data entity
            dataField.setText(element.stringProperty().get());
            // display index
            int index=elementTable.getSelectionModel().getSelectedIndex();
            indexField.setText(String.valueOf(index));
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setReadOnly(boolean readOnly) {
        isReadOnly = readOnly;
        if(isReadOnly){
            editButton.setVisible(false);
            newButton.setVisible(false);
            insertButton.setVisible(false);
            deleteButton.setVisible(false);
            editButton.setManaged(false);
            newButton.setManaged(false);
            insertButton.setManaged(false);
            deleteButton.setManaged(false);
        }
    }

    public void setDataset(Dataset dataset){
        this.dataset=dataset;
        parser=dataset.getYType().getParser();
        elements= FXCollections.observableArrayList();
        for (Object o:dataset.getElements()){
            elements.add(new DataEntity(o));
        }
        elementTable.setItems(elements);
    }

    @FXML
    private void handleOKClicked(){
        if (!isReadOnly){
            YArrayList<Object> list= new YArrayList<Object>();
            for (DataEntity dataEntity:elements){
                list.add(dataEntity.getObject());
            }
            dataset.setElements(list);
        }
        stage.close();
    }

    @FXML
    private void handleNewClicked(){
        try {
            Object o = parser.parse(dataField.getText());
            elements.add(new DataEntity(o));
        }catch (ParserException e){
        }
    }

    @FXML
    private void handleEditClicked(){
        DataEntity selected=elementTable.getSelectionModel().getSelectedItem();
        if (selected!=null)
            try {
                Object o = parser.parse(dataField.getText());
                selected.setObject(o);
            }catch (ParserException e){
            }
    }

    @FXML
    private void handleDeleteClicked(){
        int selected=elementTable.getSelectionModel().getSelectedIndex();
        if (selected>=0){
            elements.remove(selected);
        }
    }

    @FXML
    private void handleInsertClicked(){
        try {
            Object o = parser.parse(dataField.getText());
            int index=Integer.parseInt(indexField.getText());
            if (index<0 || index>=elements.size()) {
                elements.add(new DataEntity(o));
            }else{
                elements.add(index,new DataEntity(o));
            }
        }catch (ParserException | NumberFormatException ignored){
        }
    }
}
