package view;

import javafx.fxml.FXML;
import model.Dataset;
import parser.YType;
import sort_search.MainApp;
import utils.YArrayList;

import java.util.Random;

public class RootLayout {
    private MainApp main;

    public MainApp getMain() {
        return main;
    }

    public void setMain(MainApp main) {
        this.main = main;
    }

    private void insertRandomIntegerDataset(int num){
        YArrayList<Object> list=new YArrayList<>();
        Random random=new Random();
        for (int i=0;i<num;i++) list.add(random.nextInt(10000));
        Dataset dataset=new Dataset(list, YType.YInteger);
        main.getDatasets().add(dataset);
    }

    @FXML
    private void handleInt50Clicked(){
        insertRandomIntegerDataset(50);
    }

    @FXML
    private void handleInt500Clicked(){
        insertRandomIntegerDataset(500);
    }

    @FXML
    private void handleInt5000Clicked(){
        insertRandomIntegerDataset(5000);
    }
}
