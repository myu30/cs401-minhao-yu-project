package searcher;

import sort_search.SortSearch;
import utils.YArrayList;
import utils.YBinarySearchTree;

public class BinaryTreeSearcher<E> extends Searcher<E>{
    YBinarySearchTree<E> searchTree;

    public BinaryTreeSearcher(){
        this.name= SortSearch.BINARY_TREE_SEARCH;
    }

    @Override
    public void setElements(YArrayList<E> elements) {
        searchTree=new YBinarySearchTree<E>(elements);
    }

    @Override
    public boolean search(E element) {
        return searchTree.search(element);
    }

    @Override
    public int getSearchCount() {
        return searchTree.getSearchCount();
    }
}
