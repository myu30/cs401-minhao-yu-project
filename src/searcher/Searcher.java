package searcher;

import utils.YArrayList;

public abstract class Searcher<E> {
    protected String name;

    public abstract void setElements(YArrayList<E> elements);

    public abstract boolean search(E element);

    public abstract int getSearchCount();

    public int searchAll(YArrayList<E> elements){
        if (elements==null || elements.size()==0){
            return 0;
        }
        int count=0;
        for (E e:elements){
            search(e);
            count+=getSearchCount();
        }
        return count/elements.size();
    }

    public String getName() {
        return name;
    }
}
