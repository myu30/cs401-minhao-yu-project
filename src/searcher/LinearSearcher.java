package searcher;

import sort_search.SortSearch;
import utils.YArrayList;

public class LinearSearcher<E> extends Searcher<E> {
    YArrayList<E> elements;

    private int searchCount;

    public LinearSearcher(){
        this.name= SortSearch.LINEAR_SEARCH;
    }


    @Override
    public void setElements(YArrayList<E> elements) {
        this.elements=elements;
    }

    @Override
    public boolean search(E element) {
        searchCount=0;
        for (E e:elements){
            searchCount++;
            if (e.equals(element)) return true;
        }
        return false;
    }

    @Override
    public int getSearchCount() {
        return searchCount;
    }
}
