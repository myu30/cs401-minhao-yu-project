package sort_search;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Dataset;
import model.ResultEntity;
import view.EditDataDialog;
import view.MainDialog;
import view.MessageDialog;
import view.RootLayout;

import java.io.File;
import java.io.IOException;

public class MainApp extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;

    private FileChooser fileChooser;

    private MainDialog mainDialogController;
    private RootLayout rootLayoutController;

    private ObservableList<Dataset> datasets= FXCollections.observableArrayList();
    private ObservableList<ResultEntity> results= FXCollections.observableArrayList();

    public ObservableList<Dataset> getDatasets(){
        return datasets;
    }

    public ObservableList<ResultEntity> getResults() {
        return results;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage=primaryStage;
        initFileChooser();
        loadRootLayout();
        showPrimaryStage();
        showMainDialog();
    }

    private void loadRootLayout(){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/RootLayout.fxml"));
        try {
            rootLayout = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        rootLayoutController=loader.getController();
        rootLayoutController.setMain(this);
    }

    private void showPrimaryStage(){
        Scene scene=new Scene(rootLayout);
        primaryStage.setTitle(SortSearch.APP_TITLE_ROOT);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void showMainDialog(){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/MainDialog.fxml"));
        AnchorPane page= null;
        try {
            page = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        rootLayout.setCenter(page);
        mainDialogController=loader.getController();
        mainDialogController.setMain(this);
    }

    private void initFileChooser(){
        fileChooser=new FileChooser();
        fileChooser.setTitle(SortSearch.APP_TITLE_FILE_CHOOSER);
        ExtensionFilter extFilter=new ExtensionFilter("Txt file","*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
    }

    public File showInputFileDialog(){
        return fileChooser.showOpenDialog(primaryStage);
    }

    public void showMessageDialog(String title, String message){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/MessageDialog.fxml"));
        AnchorPane page= null;
        try {
            page = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage stage=new Stage();
        stage.setTitle(title);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        Scene scene=new Scene(page);
        stage.setScene(scene);

        MessageDialog controller= loader.getController();
        controller.setMessage(message);
        controller.setStage(stage);

        stage.showAndWait();

    }

    public void showEditDataDialog(Dataset dataset,boolean isReadOnly){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/EditDataDialog.fxml"));
        AnchorPane page= null;
        try {
            page = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage stage=new Stage();
        stage.setTitle(SortSearch.APP_TITLE_EDIT_DATA);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        Scene scene=new Scene(page);
        stage.setScene(scene);

        EditDataDialog controller= loader.getController();
        controller.setDataset(dataset);
        controller.setStage(stage);
        controller.setReadOnly(isReadOnly);

        stage.showAndWait();
    }

}
