package sort_search;

import searcher.BinaryTreeSearcher;
import searcher.LinearSearcher;
import searcher.Searcher;
import sorter.BubbleSorter;
import sorter.HeapSorter;
import sorter.MergeSorter;
import sorter.Sorter;
import utils.YArrayList;

public class SortSearchManager<E> {
    YArrayList<Sorter<E>> sorters;
    YArrayList<Searcher<E>> searchers;
}
