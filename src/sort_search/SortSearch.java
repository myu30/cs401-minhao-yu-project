package sort_search;

import searcher.BinaryTreeSearcher;
import searcher.LinearSearcher;
import searcher.Searcher;
import sorter.BubbleSorter;
import sorter.HeapSorter;
import sorter.MergeSorter;
import sorter.Sorter;
import utils.YArrayList;

public class SortSearch {
    public static final String LINEAR_SEARCH="Linear search";
    public static final String BINARY_TREE_SEARCH="Binary tree search";
    public static final String BUBBLE_SORT="Bubble sort";
    public static final String MERGE_SORT="Merge sort";
    public static final String HEAP_SORT="Heap sort";
    public static final String APP_TITLE_ROOT ="Sort & Search";
    public static final String APP_TITLE_FILE_CHOOSER ="Choose input data";
    public static final String APP_TITLE_EDIT_DATA="Edit Data";
    public static final String INTEGER="Integer";
    public static final String DOUBLE="Double";
    public static final String STRING="String";

    public static final int PREVIEW_NUMBER=5;
}
