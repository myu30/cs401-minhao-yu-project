package parser;

public class YDoubleParser implements YParser{
    @Override
    public Object parse(String str) {
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e){
            throw new ParserException();
        }
    }
}
