package parser;

public class YIntegerParser implements YParser {
    @Override
    public Object parse(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new ParserException();
        }
    }
}
