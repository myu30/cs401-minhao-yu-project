package parser;

import sort_search.SortSearch;

public enum YType {
    YInteger(SortSearch.INTEGER,new YIntegerParser()),
    YDouble(SortSearch.DOUBLE,new YDoubleParser()),
    YString(SortSearch.STRING,new YStringParser());

    private String name;
    private YParser parser;

    YType(String name, YParser parser){
        this.name=name;
        this.parser=parser;
    }

    public YParser getParser(){
        return parser;
    }

    public String getName(){
        return name;
    }

}
