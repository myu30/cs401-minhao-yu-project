package utils;

import javafx.scene.control.TableCell;

public class OrderCell<K,V> extends TableCell<K,V> {
    @Override
    public void updateItem(V item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty) {
            int rowIndex=this.getIndex();
            this.setText(String.valueOf(rowIndex));
        }else{
            this.setText(null);
            this.setGraphic(null);
        }
    }
}
