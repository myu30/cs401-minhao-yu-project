package utils;

import java.util.Arrays;
import java.util.Iterator;

public class YArrayList<E> implements YListInterface<E>, Iterable<E>{
    private static final int DEFAULT_SIZE=10;
    private static final int GROW_FACTOR=2;
    private E[] elements;
    private int numElements;

    public YArrayList(int size){
        elements=(E[]) new Object[size];
        numElements=0;
    }

    public YArrayList(){
        this(DEFAULT_SIZE);
    }

    public YArrayList(E[] elements, int size){
        this.elements=elements;
        this.numElements=size;
    }

    /* Methods defined by this class  */

    public boolean isSameOrder(YArrayList<E> list){
        if (numElements!=list.size())
            return false;
        for (int i=0;i<numElements;i++)
            if (!elements[i].equals(list.get(i)))
                return false;
        return true;
    }

    public void addAll(E... elements){
        for(E e:elements){
            add(e);
        }
    }

    private void grow(){
        E[] newElements = (E[]) new Object[elements.length*GROW_FACTOR];
        System.arraycopy(elements, 0, newElements, 0, numElements);
        elements=newElements;
    }

    public E[] getSubArray(int begin, int end){
        return Arrays.copyOfRange(elements,begin,end);
    }

    public YArrayList<E> copy(){
        return new YArrayList<E>(Arrays.copyOf(elements,numElements),numElements);
    }

    public void swap(int i, int j){
        E tem=elements[i];
        elements[i]=elements[j];
        elements[j]=tem;
    }

    public int compare(int i, int j){
        return YUtil.compare(elements[i],elements[j]);
    }

    /* Methods inherited from YListInterface */

    @Override
    public void add(E element) {
        if (isFull())
            grow();
        elements[numElements]=element;
        numElements++;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= numElements)
            return null;
        return(elements[index]);
    }

    @Override
    public E set(int index, E element) {
        if (index < 0 || index >= numElements)
            return null;
        E old=elements[index];
        elements[index]=element;
        return old;
    }

    @Override
    public E remove(int index) {
        if (index < 0 || index >= numElements)
            return null;
        E old=elements[index];
        for (int i=index;i<numElements-1;i++)
            elements[i]=elements[i+1];
        numElements--;
        return old;
    }

    @Override
    public boolean isFull() {
        return numElements==elements.length;
    }

    @Override
    public boolean isEmpty() {
        return numElements==0;
    }

    @Override
    public int size() {
        return numElements;
    }

    /* Methods inherited from Iterator */

    @Override
    public Iterator<E> iterator() {
        return new Itr<E>();
    }

    class Itr<T> implements Iterator<T>{
        int cur=0;
        @Override
        public boolean hasNext() {
            return cur<numElements;
        }

        @Override
        public T next() {
            return (T) elements[cur++];
        }
    }

    /* Methods inherited from Object */
    @Override
    public String toString(){
        if (numElements==0)
            return "";
        StringBuffer sb=new StringBuffer();
        for (E e:elements){
            sb.append(e.toString());
            sb.append('-');
        }
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }
}
