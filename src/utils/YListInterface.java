package utils;

public interface YListInterface<E> {

    public void add(E element);

    public E get(int index);

    public E set(int index, E element);

    public E remove(int index);

    public boolean isFull();

    public boolean isEmpty();

    public int size();
}
