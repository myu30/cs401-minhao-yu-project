package utils;

import parser.ParserException;
import parser.YParser;
import sort_search.SortSearch;

import java.io.*;

public class YUtil {
    public static <E> int compare(E o1, E o2){
        return ((Comparable<E>) o1).compareTo(o2);
    }

    public static String getPreviewString(YArrayList<Object> elements){
        StringBuffer sb=new StringBuffer();
        int size=elements.size();
        if (size> SortSearch.PREVIEW_NUMBER)
            size=SortSearch.PREVIEW_NUMBER;
        for (int i=0;i<size;i++){
            sb.append(elements.get(i));
            sb.append(',');
        }
        sb.append("...");
        return sb.toString();
    }

    public static YArrayList<Object> readFile(File file, YParser parser){
        YArrayList<Object> list=new YArrayList<Object>();
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader=new BufferedReader(reader);
            String line;
            while((line=bufferedReader.readLine())!=null) {
                try{
                    Object o=parser.parse(line);
                    list.add(o);
                } catch (ParserException e){
                }
            }
            bufferedReader.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
