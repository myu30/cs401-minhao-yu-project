package utils;

public class YBinarySearchTree<E> {
    private YBinaryTreeNode<E> root;
    private int searchCount=0;


    public YBinarySearchTree(YArrayList<E> elements){
        for (E e:elements){
            insert(e);
        }
    }

    private void insert(E element){
        YBinaryTreeNode<E> newNode= new YBinaryTreeNode<E>(element);
        if (root==null){
            root=newNode;
            return;
        }
        YBinaryTreeNode<E> currentNode=root;
        while(true){
            int compare=((Comparable<E>) element).compareTo(currentNode.getElement());
            if (compare>0){
                if (currentNode.getRight()==null){
                    currentNode.setRight(newNode);
                    break;
                }else{
                    currentNode=currentNode.getRight();
                }
            }else if (compare<0){
                if (currentNode.getLeft()==null){
                    currentNode.setLeft(newNode);
                    break;
                }else{
                    currentNode=currentNode.getLeft();
                }
            }else{
                break;
            }
        }
    }

    public boolean search(E element) {
        searchCount=0;
        if (root ==null){
            return false;
        }
        YBinaryTreeNode<E> currentNode=root;
        while (currentNode!=null){
            int compare=((Comparable<E>) element).compareTo(currentNode.getElement());
            searchCount++;
            if (compare==0){
                return true;
            }else if (compare > 0){
                currentNode=currentNode.getRight();
            }else {
                currentNode=currentNode.getLeft();
            }
        }
        return false;
    }

    public int getSearchCount(){
        return searchCount;
    }
}
