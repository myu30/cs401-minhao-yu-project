package utils;

public class YBinaryTreeNode<E> {
    private E element;
    private YBinaryTreeNode<E> left;
    private YBinaryTreeNode<E> right;

    public YBinaryTreeNode(){}

    public YBinaryTreeNode(E element){
        this.element=element;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    public YBinaryTreeNode<E> getLeft() {
        return left;
    }

    public void setLeft(YBinaryTreeNode<E> left) {
        this.left = left;
    }

    public YBinaryTreeNode<E> getRight() {
        return right;
    }

    public void setRight(YBinaryTreeNode<E> right) {
        this.right = right;
    }
}
