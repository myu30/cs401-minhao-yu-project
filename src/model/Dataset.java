package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import parser.YType;
import utils.YArrayList;
import utils.YUtil;
/**
 * This is the class of data set entity in the tableView of main dialog.
 * @author Minhao Yu
 * @date 2019/11/21
 */
public class Dataset {
    // The data set stored in this entity.
    private YArrayList<Object> elements;
    // The size of the data set.
    private final IntegerProperty size;
    // The string of data type of the data set.
    private final StringProperty type;
    // The preview of the data set.
    private final StringProperty preview;
    // The data type of the data set.
    private final YType yType;

    /* Constructors of the class. */

    public Dataset(YArrayList<Object> elements,YType yType){
        this.elements=elements;
        this.yType=yType;
        size=new SimpleIntegerProperty(elements.size());
        type=new SimpleStringProperty(yType.getName());
        preview=new SimpleStringProperty(YUtil.getPreviewString(elements));

    }

    /* Methods of the class. */

    private void updateProperty(){
        /**
         * This method update properties base on the current data set.
         * @param []
         * @return void
         */
        size.setValue(elements.size());
        preview.setValue(YUtil.getPreviewString(elements));
    }

    /* Getters and setters of the class. */

    public YType getYType(){
        return yType;
    }

    public YArrayList<Object> getElements() {
        return elements;
    }

    public void setElements(YArrayList<Object> elements){
        this.elements=elements;
        updateProperty();
    }

    public Integer getSize() {
        return size.get();
    }

    public IntegerProperty sizeProperty() {
        return size;
    }

    public StringProperty getType() {
        return type;
    }

    public StringProperty typeProperty() {
        return type;
    }

    public String getPreview() {
        return preview.get();
    }

    public StringProperty previewProperty() {
        return preview;
    }
}
