package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is the class of result entity in the result tableView of main dialog.
 * @author Minhao Yu
 * @date 2019/11/21
 */
public class ResultEntity {
    // Size of input data for operation.
    private final IntegerProperty size;
    // Name of algorithm.
    private final StringProperty algorithm;
    // Number of operations to finish the algorithm.
    private final IntegerProperty operationCount;
    // Sorted dataset
    private final Dataset dataset;

    /* Constructors of the class. */

    public ResultEntity(int size, String algorithm, int operationCount, Dataset dataset){
        this.size=new SimpleIntegerProperty(size);
        this.algorithm=new SimpleStringProperty(algorithm);
        this.operationCount=new SimpleIntegerProperty(operationCount);
        this.dataset=dataset;
    }

    /* Getters and setters of the class. */

    public Dataset getDataset() {
        return dataset;
    }

    public int getSize() {
        return size.get();
    }

    public IntegerProperty sizeProperty() {
        return size;
    }

    public String getAlgorithm() {
        return algorithm.get();
    }

    public StringProperty algorithmProperty() {
        return algorithm;
    }

    public int getOperationCount() {
        return operationCount.get();
    }

    public IntegerProperty operationCountProperty() {
        return operationCount;
    }
}
