package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This is the class of data entity in the tableView of edit data dialog.
 * @author Minhao Yu
 * @date 2019/11/21
 */
public class DataEntity {
    // The object stored in this data entity.
    private Object object;
    // The string stands for the object.
    private final StringProperty string;

    /* Constructors of the class. */

    public DataEntity(Object object){
        this.object=object;
        this.string=new SimpleStringProperty(object.toString());
    }

    /* Getters and setters of the class. */

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
        // update the string
        string.setValue(object.toString());
    }

    public StringProperty stringProperty() {
        return string;
    }
}
