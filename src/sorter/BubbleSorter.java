package sorter;

import sort_search.SortSearch;
import utils.YArrayList;
import utils.YUtil;

public class BubbleSorter<E> extends Sorter<E> {
    public BubbleSorter(){
        this.name= SortSearch.BUBBLE_SORT;
    }

    @Override
    public int sort(YArrayList<E> elements) {
        int count=0;
        for (int i=0;i<elements.size()-1;i++)
            for (int j=0;j<elements.size()-1-i;j++) {
                count++;
                if (YUtil.compare(elements.get(j),elements.get(j+1))>0){
                    elements.swap(j,j+1);
                }
            }
        return count;
    }
}
