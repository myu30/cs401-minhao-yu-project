package sorter;

import sort_search.SortSearch;
import utils.YArrayList;
import utils.YUtil;

public class MergeSorter<E> extends Sorter<E> {
    public MergeSorter(){
        this.name= SortSearch.MERGE_SORT;
    }
    private int sortCount=0;
    @Override
    public int sort(YArrayList<E> elements) {
        sortCount=0;
        sort(elements,0,elements.size()-1);
        return sortCount;
    }

    private void merge(YArrayList<E> elements,int left,int mid, int right){
        E[] larr=elements.getSubArray(left,mid+1);
        E[] rarr=elements.getSubArray(mid+1,right+1);
        int ln=mid-left+1;
        int rn=right-mid;
        int li=0;
        int ri=0;
        int i=left;
        while (li<ln && ri<rn){
            sortCount++;
            if (YUtil.compare(larr[li],rarr[ri])>0){
                elements.set(i,rarr[ri]);
                i++;
                ri++;
            }else{
                elements.set(i,larr[li]);
                i++;
                li++;
            }
        }
        while(li<ln){
            elements.set(i,larr[li]);
            i++;
            li++;
        }
        while(ri<rn){
            elements.set(i,rarr[ri]);
            i++;
            ri++;
        }
    }

    private void sort(YArrayList<E> elements,int left, int right){
        if (left>= right)
            return;
        int mid = (left+right)/2;
        sort(elements,left,mid);
        sort(elements,mid+1,right);
        merge(elements,left,mid,right);
    }
}
