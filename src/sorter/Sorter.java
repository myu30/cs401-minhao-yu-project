package sorter;

import utils.YArrayList;

public abstract class Sorter<E> {
    protected String name;


    public abstract int sort(YArrayList<E> elements);

    public String getName(){
        return name;
    }
}
