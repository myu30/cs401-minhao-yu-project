package sorter;

import sort_search.SortSearch;
import utils.YArrayList;

public class HeapSorter<E> extends Sorter<E> {
    public HeapSorter(){
        this.name= SortSearch.HEAP_SORT;
    }
    private int sortCount;
    @Override
    public int sort(YArrayList<E> elements) {
        sortCount=0;
        for (int i=elements.size()/2-1;i>=0;i--){
            updateHeap(elements,elements.size(),i);
        }
        for (int i=elements.size()-1;i>0;i--){
            elements.swap(0, i);
            updateHeap(elements,i,0);
        }
        return sortCount;
    }

    private void updateHeap(YArrayList<E> elements, int num, int top){
        int left=top*2+1;
        int right=top*2+2;
        int max=top;
        if (left<num){
            ++sortCount;
            if (elements.compare(left,max)>0)
                max=left;
        }
        if (right<num){
            ++sortCount;
            if (elements.compare(right,max)>0)
                max=right;
        }
        if (max!=top){
            elements.swap(top,max);
            updateHeap(elements,num,max);
        }
    }
}
