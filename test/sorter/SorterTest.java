package sorter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import utils.YArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SorterTest {
    static private YArrayList<Integer> list;
    static private YArrayList<Integer> target;

    @BeforeAll
    public static void init(){
        list=new YArrayList<Integer>();
        list.addAll(5,4,1,2,3);
        target=new YArrayList<Integer>();
        target.addAll(1,2,3,4,5);
    }

    @Test
    void testBubbleSort(){
        YArrayList<Integer> sortedList=list.copy();
        new BubbleSorter<Integer>().sort(sortedList);
        assertTrue(sortedList.isSameOrder(target));
    }

    @Test
    void testMergeSort(){
        YArrayList<Integer> sortedList=list.copy();
        new MergeSorter<Integer>().sort(sortedList);
        assertTrue(sortedList.isSameOrder(target));
    }

    @Test
    void testHeapSort(){
        YArrayList<Integer> sortedList=list.copy();
        new HeapSorter<Integer>().sort(sortedList);
        assertTrue(sortedList.isSameOrder(target));
    }
}