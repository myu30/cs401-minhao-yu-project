package searcher;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.YArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SearcherTest {
    static private YArrayList<Integer> list;
    static private YArrayList<Integer> notInlist;

    @BeforeAll
    public static void init() {
        list=new YArrayList<Integer>();
        list.addAll(5,4,1,2,3);
        notInlist=new YArrayList<Integer>();
        notInlist.addAll(0,6,7,8,9,10);
    }

    @Test
    void testLinearSearcher(){
        boolean pass=true;
        Searcher<Integer> searcher=new LinearSearcher<Integer>();
        searcher.setElements(list);
        for (Integer i:list){
            if (!searcher.search(i))
                pass=false;
        }
        for (Integer i:notInlist){
            if (searcher.search(i))
                pass=false;
        }
        assertTrue(pass);
    }

    @Test
    void testBinaryTreeSearcher(){
        boolean pass=true;
        Searcher<Integer> searcher=new BinaryTreeSearcher<Integer>();
        searcher.setElements(list);
        for (Integer i:list){
            if (!searcher.search(i))
                pass=false;
        }
        for (Integer i:notInlist){
            if (searcher.search(i))
                pass=false;
        }
        assertTrue(pass);
    }



}